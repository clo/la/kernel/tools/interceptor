# Interceptor

**WARNING**: The interceptor is still a work in progress. Contents of this
section are subject to change.

The interceptor is a tool to intercept all `execve` and other system calls
during the build (specifically, `make`) in order to construct the dependency
tree and understand a list of commands executed by the build system. With the
interceptor, the Bazel build system may first "dry-run", analyze all inputs and
outputs of each command, generate `BUILD.bazel` files for each command, then
execute on these generated build files.

This allows us to enable the following:

* Trim the dependency tree for each command.
    * Reduce the possibility of invalidating an output when an unrelated input
      file is touched
    * More parallelism within Bazel, not make
    * Even faster incremental builds
* Generate
  [compile\_commands.json](https://clang.llvm.org/docs/JSONCompilationDatabase.html),
  which can then be plugged into IDEs and code search tools (e.g. https://cs.android.com/android/kernel/superproject)
* More cache hits for Remote Build Execution (RBE)
* Give a more trimmed and precise list of resources for DDK
* etc.
